const formulario = document.querySelector('#formulario-contacto');
const inputs = document.querySelectorAll('#formulario-contacto input');
const textarea = document.querySelector('#formulario-contacto textarea');

const expresiones = {
  name: /^[a-zA-ZñÑ\s]+$/,
  email: /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/,
  subject: /^[0-9a-zA-ZñÑ\s]+$/,
  message: /^[0-9a-zA-ZñÑ\s]+$/
};

const campos = {
  name: false,
  email: false,
  subject: false,
  message: false
};

const validarFormularioInput = (e) => {
  //Accedemos al valor que tiene el id
  //console.log(e.target.id);

  switch(e.target.id){
    case 'name':
      validarCampoInput(expresiones.name, e.target, 'name');
      break;
    case 'email':
      validarCampoInput(expresiones.email, e.target, 'email');
      break;
  }
};

const validarCampoInput = (expresion, input, campo) => {
  //Hacemos una evaluacion de la expresion regular
  //Con value accedemos al input

  if(expresion.test(input.value)){
    //Remueve la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.remove('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove('formulario-input-error-activo');
    campos[campo] = true;
  } else {
    //Adiciona la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.add('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add('formulario-input-error-activo');
    campos[campo] = false;
  }
};

const validarFormularioTextArea = (e) => {
  //console.log(e.target.id);

  if(expresiones.message.test(e.target.value)){
    //Remueve la clase
    document.querySelector('#grupo-message #message').classList.remove('form-control-error');
    document.querySelector('#grupo-message .formulario-input-error').classList.remove('formulario-input-error-activo');
    campos['message'] = true
  } else {
    //Adiciona la clase
    document.querySelector('#grupo-message #message').classList.add('form-control-error');
    document.querySelector('#grupo-message .formulario-input-error').classList.add('formulario-input-error-activo');
    campos['message'] = false;
  }
};

inputs.forEach((input) => {
  //console.log(input);
  /*
  keyup cuando se presiona la tecla y se levanta el dedo, el evento se ejecuta
  */
  input.addEventListener('keyup', validarFormularioInput);
  /*blur es disparado cuando un elemento ha perdido su foco*/
  input.addEventListener('blur', validarFormularioInput);
});

textarea.addEventListener('keyup', validarFormularioTextArea);

textarea.addEventListener('blur', validarFormularioTextArea);

formulario.addEventListener('submit', async (e) => {
  e.preventDefault();

  console.log('ingreso al boton');

  if(campos.nombre && campos.email && campos.mensaje){
    let name = document.querySelector('#name');
    let email = document.querySelector('#email');
    let message = document.querySelector('#message');

    name = name.value.trim();
    email = email.value;
    message = message.value.trim();

    console.log(name);
    console.log(email);
    console.log(message);

  }
});

